#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""ACoCL project utilitis.

Utility functions and procedures for ACoCL projects.
"""

import sys, getopt, os
import logging, logging.handlers
import getpass
import yaml
from datetime import datetime
from os.path import expanduser


def setup_logging(logger, settings):
    """Log output

        Sends log output to console or file,
        depending on error level
    """
    try:
        log_filename = os.path.join(
            os.path.abspath(
                __file__+os.path.sep+'..'+os.path.sep+'..'
            ),
            settings['log_filename']
        )
        log_max_bytes = settings['log_max_bytes']
        log_backup_count = settings['log_backup_count']
    except KeyError as ex:
        print "WARNING: Missing logfile setting {}. Using defaults.".format(ex)
        log_filename = os.path.join(
            os.path.dirname(os.path.realpath(__file__)),
            "log.txt"
        )
        log_max_bytes = 1048576 #1MB
        log_backup_count = 5

    logger.setLevel(logging.DEBUG)
    # Set up logging to file
    file_handler = logging.handlers.RotatingFileHandler(
        filename=log_filename,
        maxBytes=log_max_bytes,
        backupCount=log_backup_count
    )
    file_handler.setLevel(logging.DEBUG)
    file_formatter = logging.Formatter(
        '%(asctime)s %(name)-15s %(levelname)-8s %(message)s',
        '%m-%d %H:%M'
    )
    file_handler.setFormatter(file_formatter)
    logger.addHandler(file_handler)

    # Handler to write INFO messages or higher to sys.stderr
    console_handler = logging.StreamHandler()
    console_handler.setLevel(logging.INFO)
    console_formatter = logging.Formatter('...%(message)s')
    console_handler.setFormatter(console_formatter)
    logger.addHandler(console_handler)
    logger.debug("---------------------------------")
    logger.debug(
        "Starting log for session %s",
        datetime.now().strftime("%Y%m%d%H%M%S%f")
    )


def get_settings(settings_file):
    """Get settings from external YAML file
    """
    settings = {}
    try:
        with open(settings_file, 'r') as ymlfile:
            settings = yaml.load(ymlfile)
    except IOError:
        print "Could not open settings file {}".format(settings_file)
        sys.exit(1)
    return settings


def print_header(header_text):
    """Print the output header to the command line.
    """
    print "\n=================================================="
    print str(header_text)
    print "=================================================="


def print_usage():
    """Print usage instructions to the screen.

    For the usage format, see http://en.wikipedia.org/wiki/Usage_message.
    """
    print """\
Usage: script.py [-h --help | -a=arg1|arg2|arg3] [-b=optb]

Options:
-a arg, --option arg
    Desciption of option
    
-b arg, --option arg
    Description of option

-h, --help
    Display options

Arguments:
arg1    : Description of arg
arg2    : Description of arg
arg3    : Description of arg

"""


def query_yes_no(question, default="yes"):
    """Ask a yes/no question via raw_input() and return their answer.

    Args:
        question (string): is a string that is presented to the user.
        default (string): is the presumed answer if the user just hits <Enter>.
            It must be "yes" (the default), "no" or None (meaning
            an answer is required of the user).

    The "answer" return value is True for "yes" or False for "no".

    This code is a recipe from http://code.activestate.com/recipes/577058/.
    """
    valid = {"yes": True, "y": True, "ye": True,
             "no": False, "n": False}
    if default is None:
        prompt = " [y/n] "
    elif default == "yes":
        prompt = " [Y/n] "
    elif default == "no":
        prompt = " [y/N] "
    else:
        raise ValueError("invalid default answer: '%s'" % default)

    while True:
        sys.stdout.write(question + prompt)
        choice = raw_input().lower()
        if default is not None and choice == '':
            return valid[default]
        elif choice in valid:
            return valid[choice]
        else:
            sys.stdout.write("Please respond with 'yes' or 'no' "
                             "(or 'y' or 'n').\n")

def ask_credentials():
    """Ask for username and password credentials.
    """
    user = raw_input("Username: ")
    if user:
        passwd = getpass.getpass("Password for " + user + ": ")
    else:
        user = None
        passwd = None

    return (user, passwd)

