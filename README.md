# DeliciousPy 

A utility for importing Delicious bookmarks into a MySQL database.

## License
Written by Anthony Lopez-Vito of [Another Cup of Coffee Limited](http://anothercoffee.net). All code is released under The MIT License.
Please see LICENSE.txt.

## Credits
Netscape bookmark parser `netscape_bookmarks.py` copyright (c) 2013, Thibauld Nion.

Licensed under 2-clause BSD. (See source code file for copyright
notice, this list of conditions and the following disclaimer.)

`netscape_bookmarks.py` is part of the **Water On Mars!** project which intends to provide a free (as in freedom) alternative to web news readers and bookmarking apps.


* [Thibauld Nion's GitHub profile](https://github.com/tibonihoo)
* [Thibauld Nion's blog](http://www.tibonihoo.net/)
* [Water On Mars!](https://github.com/tibonihoo/wateronmars)