#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""Import Delicious bookmarks into a MySQL database.

Takes a html file exported from Delicious and imports the contents
into a MySQL database.
"""

import sys, getopt, os
import logging, logging.handlers
import cgi
import yaml
from acoclutils.utils import get_settings
from acoclutils.utils import setup_logging 
from acoclutils.netscape_bookmarks import parse_netscape_bookmarks
from datetime import datetime
from os.path import expanduser
from contextlib import closing
import MySQLdb as mdb
from MySQLdb import OperationalError
from subprocess import check_call

logger = logging.getLogger()

QUERY_RESET_TABLES = """DROP TABLE IF EXISTS delicious.bookmarks;

CREATE TABLE delicious.bookmarks (
  `ID` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `date_add` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `modified` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',  
  `title` text NOT NULL,
  `url` varchar(255) NOT NULL DEFAULT '',
  `note` longtext NOT NULL,  
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

DROP TABLE IF EXISTS delicious.tags;
CREATE TABLE delicious.tags (
  `tag_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(200) NOT NULL DEFAULT '',
  `group` bigint(10) NOT NULL DEFAULT '0',
  PRIMARY KEY (`tag_id`),
  KEY (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

DROP TABLE IF EXISTS delicious.tag_relationships;
CREATE TABLE delicious.tag_relationships (
  `item_id` bigint(20) unsigned NOT NULL DEFAULT '0',
  `tag_id` bigint(20) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`item_id`,`tag_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
"""

QUERY_INSERT_ITEM = """INSERT INTO delicious.bookmarks (
        date_add,
        title,
        url,
        note
    )
    VALUES (FROM_UNIXTIME(%s), %s, %s, %s);
        """

QUERY_INSERT_TAG = "REPLACE INTO delicious.tags (name) VALUES(LOWER(%s));"

QUERY_TAG = "SELECT tag_id FROM delicious.tags WHERE name=%s;"

QUERY_INSERT_TAG_RELATIONSHIP = """INSERT INTO delicious.tag_relationships (
        item_id,
        tag_id
    )
    VALUES (%s, %s);
    """

def print_usage():
    """Print usage instructions to the screen.

    For the usage format, see http://en.wikipedia.org/wiki/Usage_message.
    """
    print """\
    Usage: projectsetup.py [-h --help | -p=path]

    Options:
    -p path, --path path
        Path to the project

    -h, --help
        Display options
    """    


def show_instructions():
    print ("Setup is complete.")


def read_file(filename):
    logging.debug("Processing %s", filename)
    if filename == '':
        logging.critical("File not found: %s", filename, exc_info=True)
        exit()
    content = ''
    if os.path.isfile(filename):
        f = open(filename, 'r')
        content = f.read()
    else:
        logging.critical("File not found", exc_info=True)
    f.close()

    return content


def querydb(dbconn, query, params):
    """Run a MySQL query string.

    Args:
        query (string): MySQL query string.

    Returns:
        results: Results of the query as a list of tuples.
    """
    results = None
    with closing(dbconn.cursor(mdb.cursors.DictCursor)) as cur:
        try:
            cur.execute(query, params)
            results = cur.fetchall()
        except (mdb.OperationalError, mdb.ProgrammingError), e:
            logging.error("There was a problem while trying to run a query:\n\t%s", e[1])
            logging.error(query)
            cur.close()
            raise
        except mdb.Warning, warn:
            logging.warn("Warning: %s", warn)
            raise
    return results


def insert_item(mysqldbconn, bookmark_item):
    """
    Adds a bookmark item to the database.
    
    Args:
        bookmark_item: Dictionary of bookmark information

    Returns:
        item_id: ID of inserted item or 0 if insert failed
    """
    logging.info("Inserting: %s", bookmark_item["title"])
    item_id = 0
    try:    
        cur = mysqldbconn.cursor()
        
        params = (
            bookmark_item["date_add"],
            bookmark_item["title"],
            bookmark_item["url"],
            bookmark_item["note"]
        )
        cur.execute(QUERY_INSERT_ITEM, params)
        item_id = cur.lastrowid
        mysqldbconn.commit()

    except mdb.Error, e:
        success = False
        logging.error("Sorry there was an error %s: %s", e[0], e[1])
        logging.warn("Unable to add item; rollback called")
        mysqldbconn.rollback()
        cur.close()
    return item_id


def process_tags(mysqldbconn, bookmark_id, tags):    
    tag_list = tags.split(",")
    
    for tag in tag_list:
        if tag:
            tag_id = insert_tag(mysqldbconn, tag)
            insert_tag_relationship(mysqldbconn, bookmark_id, tag_id)


def insert_tag(mysqldbconn, tag):
    tag_id = get_tag_id(mysqldbconn, tag)
    if not tag_id:
        logging.info("Inserting tag: %s", tag)    
        try:
            cur = mysqldbconn.cursor()
            params = (tag,)
            result = cur.execute(QUERY_INSERT_TAG, params)
            mysqldbconn.commit()
            tag_id = get_tag_id(mysqldbconn, tag)

        except mdb.Error, e:
            success = False
            logging.error("Sorry there was an error %s: %s", e[0], e[1])
            logging.warn("Unable to add tag; rollback called")
            mysqldbconn.rollback()
            cur.close()
    return tag_id    

def get_tag_id(mysqldbconn, tag):
    """Retrieve tag ID from database.

    Returns:
        tag_id: Returns tag ID if exists; 0 if doesn't exist.
    """
    logging.info("Getting ID for tag %s", tag)
    tag_id=0
    try:
        params = (tag,)
        results =  querydb(mysqldbconn,QUERY_TAG, params)
        if results:
            tag_id = results[0]['tag_id']

    except mdb.Error, e:
        success = False
        logging.error("Sorry there was an error %s: %s", e[0], e[1])
        logging.warn("Unable to add tag; rollback called")
        mysqldbconn.rollback()
        cur.close()    
    return tag_id


def insert_tag_relationship(mysqldbconn, bookmark_id, tag_id):
    logging.info("Exporting item-tag relationships...")
    try:
        logging.info("Exporting item-tag relationship [%s] [%s]", bookmark_id, tag_id)

        cur = mysqldbconn.cursor()
        params = (bookmark_id, tag_id)
        cur.execute(QUERY_INSERT_TAG_RELATIONSHIP, params)
        mysqldbconn.commit()

    except mdb.Error, e:
        success = False
        logging.error("Sorry there was an error %s: %s", e[0], e[1])
        logging.warn("Unable to add tag relationship; rollback called")
        mysqldbconn.rollback()
        cur.close()


def export_bookmarks(settings, filename):
    logging.info("Starting. Please wait...")
    
    try:
        mysqldbconn = mdb.connect(
            settings['database']['host'],
            settings['database']['admin_username'],
            settings['database']['admin_password'],
            charset='utf8mb4',
            use_unicode=True
        )
    except OperationalError:
        logging.error("Could not access the database. Aborting database creation.")
    else:
        logging.info("Starting script...")
        
        logging.info("Resetting tables...")
        results = querydb(mysqldbconn, QUERY_RESET_TABLES, [])
    
    content = read_file(filename)
    counter = 0
    bookmarks = parse_netscape_bookmarks(content)
    logging.debug("Found %s bookmarks", len(bookmarks))
    for bookmark in bookmarks:
        bookmark_item = {}
        bookmark_item["date_add"] = bookmark["posix_timestamp"]
        bookmark_item["title"] = bookmark.get("title","<no title>")
        bookmark_item["url"] =bookmark["url"]
        bookmark_item["private"] = bookmark["private"]
        bookmark_item["note"] = bookmark.get("note","")
        item_id = insert_item(mysqldbconn, bookmark_item)

        try:
            process_tags(mysqldbconn, item_id, bookmark["tags"])
        except KeyError:
            logging.debug("No tags for this item")

        logging.debug("---")
        counter +=1 
    logging.info("Count: %s", counter)


def main(argv):
    """Process the user's commands.

    Args:
        argv: The command line options.
    """
    settings_file = os.path.join(
        os.path.dirname(os.path.realpath(__file__)),
        "settings.yml"
    )

    settings = get_settings(settings_file)
    setup_logging(logger, settings['logger'])

    try:
        opts, args = getopt.getopt(
            argv,
            "p:h",
            ["path=", "help"]
        )
    except getopt.GetoptError:
        print_usage()
        sys.exit(2)

    arguments = dict()
    # Get command line options and arguments
    if len(opts) != 0:
        for opt, arg in opts:
            if opt in ("-h", "--help"):
                print_usage()
                sys.exit()
            elif opt in ("-p", "--path"):
                export_bookmarks(settings, arg)
    else:
        print "Please supply a project path. See -h."


# Program entry point
if __name__ == "__main__":
    main(sys.argv[1:])
