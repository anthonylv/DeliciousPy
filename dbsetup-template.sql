CREATE DATABASE IF NOT EXISTS delicious CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;

GRANT SELECT, INSERT, UPDATE, DELETE, CREATE, DROP, INDEX, ALTER, CREATE TEMPORARY TABLES, LOCK TABLES ON delicious.* TO 'user'@'localhost' IDENTIFIED BY 'PASSWORD';
FLUSH PRIVILEGES;

DROP TABLE IF EXISTS delicious.bookmarks;

CREATE TABLE delicious.bookmarks (
  `ID` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `date_add` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `modified` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',  
  `title` text NOT NULL,
  `url` varchar(255) NOT NULL DEFAULT '',
  `note` longtext NOT NULL,  
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

DROP TABLE IF EXISTS delicious.tags;
CREATE TABLE delicious.tags (
  `tag_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(200) NOT NULL DEFAULT '',
  `group` bigint(10) NOT NULL DEFAULT '0',
  PRIMARY KEY (`tag_id`),
  KEY (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

DROP TABLE IF EXISTS delicious.tag_relationships;
CREATE TABLE delicious.tag_relationships (
  `item_id` bigint(20) unsigned NOT NULL DEFAULT '0',
  `tag_id` bigint(20) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`item_id`,`tag_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
